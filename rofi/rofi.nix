{ config, pkgs, lib, ... }:

let
  link = config.lib.file.mkOutOfStoreSymlink;
in
{
  home.file.".config/rofi/config".source = link "/home/noone/.dotfiles/rofi/config";

  home.packages = with pkgs; [
    rofi
  ];
}
