#!/bin/sh

read -r "Enter backup path: " dst

sync_folder() {
    folder=$1

    in="/home/noone/$folder"
    out="$dst/$folder"

    echo "Syncing $folder"
    rsync -av --exclude 'node_modules' "$in" "$out"
}

sync_folder ".dotfiles"
sync_folder ".ssh"
sync_folder ".thunderbird"
sync_folder "Dev"
sync_folder "Documents"
sync_folder "Music"
sync_folder "Videos"
