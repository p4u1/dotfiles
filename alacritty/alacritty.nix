{ config, pkgs, lib, ... }:

let
  link = config.lib.file.mkOutOfStoreSymlink;
in
{
  home.file.".config/alacritty/alacritty.toml".source = ./alacritty.toml;

  programs.alacritty.enable = true;
  programs.dircolors.enable = true;

  home.packages = with pkgs; [
    # Javascript / Typescript
    yarn
    nodejs_20
    nodePackages.typescript

    # Golang
    unstable.go_1_22
    golint
    # atlas

    # C / C++
    gcc
    gnumake
    cmake
    gdb

    # Tools
    less
    keychain
    openssl
    docker-compose
    envsubst
    wget
    tmux
    tmuxinator
    unstable.kubectl
    unstable.kubecolor
    unstable.kustomize
    kubent
    unstable.k6
    htop
    fzf
    jq
    ripgrep
    fd
    nmap
    ansible
    tree
    xclip
    xorg.xhost
    restic
    (bats.withLibraries (p: [
        p.bats-support
        p.bats-assert
        bats-file-0-4
    ]))

    hugo
    pass
  ];
}
