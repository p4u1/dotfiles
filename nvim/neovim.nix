{ config, pkgs, lib, ... }:

let
  link = config.lib.file.mkOutOfStoreSymlink;
in
{
  home.file.".config/nvim/main.lua".source = link "/home/noone/.dotfiles/nvim/main.lua";
  home.file.".config/nvim/ftplugin/go.vim".source = link "/home/noone/.dotfiles/nvim/ftplugin/go.vim";
  home.file.".config/nvim/ftplugin/todo.lua".source = link "/home/noone/.dotfiles/nvim/ftplugin/todo.lua";
  home.file.".config/nvim/lua/noone/actions.lua".source = link "/home/noone/.dotfiles/nvim/lua/noone/actions.lua";
  home.file.".config/nvim/lua/noone/lsputils.lua".source = link "/home/noone/.dotfiles/nvim/lua/noone/lsputils.lua";
  home.file.".config/nvim/lua/noone/plugin.lua".source = link "/home/noone/.dotfiles/nvim/lua/noone/plugin.lua";
  home.file.".config/nvim/lua/noone/terminal.lua".source = link "/home/noone/.dotfiles/nvim/lua/noone/terminal.lua";
  home.file.".config/nvim/lua/noone/utils.lua".source = link "/home/noone/.dotfiles/nvim/lua/noone/utils.lua";
  home.file.".config/nvim/lua/noone/window.lua".source = link "/home/noone/.dotfiles/nvim/lua/noone/window.lua";

  programs.neovim.enable = true;
  programs.neovim.package = pkgs.neovim-unwrapped;
  programs.neovim.extraConfig = ''
    source ~/.config/nvim/main.lua
  '';

  programs.neovim.plugins = with pkgs; [
    # pkgs.unstable.vimPlugins.nvim-plenary
    (vimPlugins.nvim-treesitter.withPlugins (p: with p; [
      tree-sitter-bash
      tree-sitter-c
      tree-sitter-go
      tree-sitter-html
      tree-sitter-javascript
      tree-sitter-json
      tree-sitter-lua
      tree-sitter-nix
      tree-sitter-regex
      tree-sitter-yaml
      tree-sitter-norg
    ]))
    vimPlugins.vim-startify
    unstable.vimPlugins.trouble-nvim

    # telescope
    vimPlugins.telescope-nvim
    vimPlugins.telescope-fzf-native-nvim

    # completion
    vimPlugins.luasnip
    vimPlugins.nvim-compe
    vimPlugins.nvim-cmp
    vimPlugins.cmp-buffer
    vimPlugins.cmp-path
    vimPlugins.cmp-nvim-lsp
    vimPlugins.cmp_luasnip
    vimPlugins.lspkind-nvim

    # explorer/workspace
    vimPlugins.nerdtree
    vimPlugins.vim-rooter

    # lsp
    vimPlugins.nvim-lspconfig
    /* nvim-code-action-menu */

    # git
    vimPlugins.git-messenger-vim
    vimPlugins.gitsigns-nvim

    vimPlugins.editorconfig-vim

    vimPlugins.vim-surround
    vimPlugins.vim-repeat
    vimPlugins.comment-nvim

    vimPlugins.lualine-nvim
    vimPlugins.nvim-web-devicons
    vimPlugins.catppuccin-nvim
    vimPlugins.vim-numbertoggle # disables relative line numbers for non current buffer
    vimPlugins.nvim-ts-autotag
    vimPlugins.todo-comments-nvim
    vimPlugins.neodev-nvim
    # vim-tpipeline # Unfortunately its too buggy atm
    vimPlugins.fwatch-nvim
    vimPlugins.neorg
  ];

  programs.neovim.extraPackages = with pkgs; [
    # For LSP
    gopls
    nixd
    efm-langserver
    luajitPackages.lua-lsp
    nodePackages.prettier
    nodePackages.typescript-language-server
    nodePackages.typescript
    nodePackages.dockerfile-language-server-nodejs
    nodePackages.yaml-language-server
    nodePackages.bash-language-server
    vscode-langservers-extracted
    phpactor
    clang-tools # for ccls

    # For Telescope
    ripgrep
    fd
    bat
    fzy
    php
  ];
}
