-- HELPERS {{{
P = function(v)
  print(vim.inspect(v))
  return v
end

local actions = require("noone/actions")

local function map(mode, l, r, opts)
  opts = opts or {}
  vim.keymap.set(mode, l, r, opts)
end

-- }}}

-- GENERAL SETTINGS {{{
vim.opt.mouse = "a" -- enable mouse

-- set shortmess+=c
vim.opt.wildignorecase = true
-- set wildignore+=*/.git/**/*
-- set wildignore+=tags
-- set wildignorecase

vim.opt.splitright = true
vim.opt.splitbelow = true

-- inccommand
vim.opt.inccommand = "split"

-- scroll
vim.opt.scrolloff = 10

-- tabs, spaces
vim.opt.expandtab = true
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4

-- folding
vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
vim.opt.foldenable = false

-- search
vim.opt.ignorecase = true
vim.opt.smartcase = true


vim.opt.updatetime = 250 -- Decrease update time

vim.wo.signcolumn = "yes:1"

-- vim.opt.listchars = { 'tab:▸ ,extends:❯,precedes:❮,trail:' }
vim.opt.fillchars = "eob:█"
vim.opt.showbreak = "↪"

-- swap, undo, backup
vim.opt.swapfile = false
vim.opt.backupdir = os.getenv("HOME") .. "/.local/share/nvim/backup/"
vim.opt.undofile = true
vim.opt.undofile = true

vim.opt.list = true
vim.cmd "au BufRead,BufNewFile *.nix set filetype=nix"
-- }}}
-- GENERAL MAPPINGS {{{
vim.g.mapleader = " "
vim.g.maplocalleader = "ö"

function _G.reload_nvim_conf()
  for name, _ in pairs(package.loaded) do
    if name:match('^noone') then
      package.loaded[name] = nil
    end
  end

  dofile(vim.env.MYVIMRC)
  vim.notify("Nvim configuration reloaded!", vim.log.levels.INFO)
end

map("n", "<leader>w", ":w<CR>") -- save file
map("n", "<leader>sw", ":w !sudo -S tee %<CR>") -- save file using sudo
map("n", "<leader>rr", ":source %<CR>") -- reload current file
-- map("n", "<leader>ir", ":source ~/.config/nvim/main.lua<CR>") -- reload main.lua
map("n", "<leader>ir", "<cmd>lua reload_nvim_conf()<CR>") -- reload main.lua
map("n", "<leader>ie", ":edit ~/.config/nvim/main.lua<CR>") -- reload main.lua
map("n", "<ESC>", ":noh<CR>:cclose<CR>") -- stop search highlighting and close quickfix
map("n", "gh", ":help <C-r><C-w><CR>") -- goto help file for word under curor
map("n", "gx", ":execute 'silent! !xdg-open ' . shellescape(expand('<cWORD>'), 1)<cr>", { noremap = true, silent = true })

map("n", "<Tab>", ":bnext<CR>")
map("n", "<S-Tab>", ":bprev<CR>")

map("n", "<leader>x", ":luafile %<CR>")

-- sort selected lines
map("v", "<leader>s", ":sort<CR>")

-- window movement
map("n", "<C-h>", "<C-w>h")
map("n", "<C-j>", "<C-w>j")
map("n", "<C-k>", "<C-w>k")
map("n", "<C-l>", "<C-w>l")
map("i", "<C-h>", "<ESC><C-w>h")
map("i", "<C-j>", "<ESC><C-w>j")
map("i", "<C-k>", "<ESC><C-w>k")
map("i", "<C-l>", "<ESC><C-w>l")

-- faster movement
map("n", "<M-h>", "^")
map("n", "<M-j>", "5j")
map("n", "<M-k>", "5k")
map("n", "<M-l>", "$")

map("v", "<M-h>", "^")
map("v", "<M-j>", "5j")
map("v", "<M-k>", "5k")
map("v", "<M-l>", "$")

map("t", "<M-h>", "^")
map("t", "<M-j>", "5j")
map("t", "<M-k>", "5k")
map("t", "<M-l>", "$")
-- }}}

-- WINDOW {{{
local window = require("noone.window")
window.setup {
  top = {
    location = "top",
    size = 20
  },
  bottom = {
    location = "bottom",
    size = 20
  },
  left = {
    location = "left",
    size = 40
  },
  right = {
    location = "right",
    size = 80
  }
}
map("n", "<A-v>", function() window.toggle("left") end)
map("n", "<A-b>", function() window.toggle("bottom") end)
map("n", "<A-n>", function() window.toggle("top") end)
map("n", "<A-m>", function() window.toggle("right") end)
-- }}}
-- TERMINAL {{{
map("t", "<A-Esc>", "<C-\\><C-n>")
vim.cmd [[ autocmd TermOpen * setlocal nocursorline ]]

local terminal = require("noone/terminal")
for i = 1, 10, 1 do
  local termname = string.format("term%d", i)
  terminal.setup {
    [termname] = {
      window = "bottom",
      interactive = true,
      mappings = {
        n = {
          ["gf"] = actions.open_file_under_cursor
        }
      }
    }
  }
  map("n", string.format("<A-%s>", i), function() terminal.toggle(termname) end)
  map("t", string.format("<A-%s>", i), function() terminal.toggle(termname) end)
end
map("n", "<A-t>", function() terminal.toggle("term1") end)
map("t", "<A-t>", function() terminal.toggle("term1") end)
map("n", "<A-r>", function() terminal.run_current_line("term1") end)
map("v", "<A-r>", function() terminal.run_selection("term1") end)
-- }}}

-- TREESITTER {{{
local treeseitter = require("nvim-treesitter.configs")
treeseitter.setup {
  highlight = { enable = true },
  indent = { enable = true },
  autotag = { enable = true },
}
-- }}}

-- UI START SCREEN {{{
vim.g.startify_lists = {
  { type = "dir", header = { "   MRU" } },
  { type = "commands", header = { "   Commands" } }
}

vim.g.startify_commands = {
  { g = { "Find Git Files", "lua require('telescope.builtin').git_files()" } },
}
-- }}}

-- UI COLORSCHEME {{{
vim.opt.termguicolors = true -- enable termguicolor
-- Needed to ensure float background doesn't get odd highlighting
-- https://github.com/joshdick/onedark.vim#onedarkset_highlight
-- vim.cmd [[augroup colorset]]
-- vim.cmd [[autocmd!]]
-- vim.cmd [[autocmd ColorScheme * call onedark#set_highlight("Normal", { "fg": { "gui": "#ABB2BF", "cterm": "145", "cterm16" : "7" } })]]
-- vim.cmd [[augroup END]]
-- vim.cmd "colorscheme onelight" -- colorscheme
vim.cmd "colorscheme catppuccin"
vim.o.background = "dark"
local fwatch = require("fwatch")
fwatch.watch("/home/noone/.config/nvim/theme.lua", ":source ~/.config/nvim/theme.lua")
-- }}}
-- UI LINE NUMBERS {{{
vim.opt.number = true -- enable line number
vim.opt.relativenumber = true -- enable relative line numbers
-- }}}
-- UI CURSOR {{{
vim.opt.cursorline = true -- enable line number
-- }}}
-- UI YANK {{{
-- highlights yanked text
vim.cmd [[ augroup highlight_yank ]]
vim.cmd [[   autocmd! ]]
vim.cmd [[   autocmd TextYankPost * silent! lua require'vim.highlight'.on_yank() ]]
vim.cmd [[ augroup END ]]
-- }}}
-- UI STATUSLINE & COMMANDLINE {{{
-- vim.opt.laststatus = 3
-- vim.g.tpipeline_cursormoved = 1 -- Unfortunately its too buggy atm
local lualine = require("lualine")
lualine.setup(
  {
    options = {
      theme = "catppuccin",
      section_separators = { left = "", right = "" },
      component_separators = { left = "", right = "" },
      icons_enabled = false
    },
  }
)
-- }}}

-- UTIL: COMMENTS {{{
require('Comment').setup()
-- }}}
-- UTIL: COMPLETION {{{
vim.opt.completeopt = "menuone,noinsert,noselect" -- wildmenu, completion

local lspkind = require('lspkind')
local cmp = require('cmp')
cmp.setup {
  sources = {
    { name = 'nvim_lsp' },
    { name = 'path' },
    { name = 'buffer' },
    { name = 'luasnip' },
    { name = 'neorg' },
  },
  formatting = {
    format = function(_, vim_item)
      vim_item.kind = lspkind.presets.default[vim_item.kind]
      return vim_item
    end
  },
  snippet = {
    expand = function(args)
      require('luasnip').lsp_expand(args.body)
    end,
  },
  mapping = {
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<CR>'] = cmp.mapping.confirm({ select = true }),
    ['<C-e>'] = cmp.mapping.abort(),
    ['<C-p>'] = cmp.mapping.select_prev_item(),
    ['<c-n>'] = cmp.mapping.select_next_item(),
  }
  -- mapping = {
  --   ['<C-d>'] = cmp.mapping.scroll_docs(-4),
  --   ['<C-f>'] = cmp.mapping.scroll_docs(4),
  --   ['<C-Space>'] = cmp.mapping.complete(),
  --   ['<CR>'] = cmp.mapping.confirm({ select = true }),
  -- },
}
-- }}}
-- UTIL ROOTER {{{
vim.g.rooter_patterns = { '.git' }
-- }}}
-- UTIL TROUBLE LIST {{{
local trouble = require("trouble")
trouble.setup {
  mode = "quickfix",
  auto_preview = false,
  height = 20
}

map("n", "<A-q>", function ()
  if trouble.is_open() then
      trouble.close()
  else
      window.open("bottom");
      local win = vim.api.nvim_get_current_win()
      trouble.open({ win = win })
  end
end)
-- map("n", "<C-n>", function() trouble.next({ skip_groups = false, jump = true }) end)
-- map("n", "<C-p>", function() trouble.previous({ skip_groups = false, jump = true }) end)
map("n", "<C-n>", ":cnext<CR>")
map("n", "<C-p>", ":cprev<CR>")

map("n", "<leader>dd", ":Trouble lsp_workspace_diagnostics<CR>")
--}}}
-- UTIL EXPLORER {{{
vim.g.NERDTreeShowHidden = 1
vim.g.NerdTreeWinSize = 70

map("n", "<A-e>", ":NERDTreeToggle<CR>", { silent = true })

vim.cmd "au FileType nerdtree :silent nmap <buffer> a o"
-- }}}
-- UTIL TELESCOPE {{{
local telescope = require("telescope")
local tel_builtin = require("telescope.builtin")
local tel_actions = require("telescope.actions")
local tel_action_set = require("telescope.actions.set")
local tel_action_state = require("telescope.actions.state")
local tel_sorters = require("telescope.sorters")
local tel_trouble = require("trouble.providers.telescope")

local function tel_actions_send_selected_to_qflist(...)
  tel_actions.send_selected_to_qflist(...)
  tel_actions.open_qflist(...)
  window.close("bottom")
  -- quickfix.open("qf")
end

local function telescope_to_trouble(...)
  tel_actions.send_to_qflist(...)
  tel_actions.open_qflist(...)
  -- tel_trouble.open_with_trouble(...)
  window.close("bottom")
end

telescope.setup {
  defaults = {
    file_sorter = tel_sorters.get_fzf_sorter,
    file_ignore_patterns = { ".git/", "*bundle.js" },
    layout_strategies = "horizontal",
    vimgrep_arguments = {
      "rg",
      "--color=never",
      "--no-heading",
      "--with-filename",
      "--line-number",
      "--column",
      "--smart-case",
      "--hidden",
      -- '-g "!.git"',
      -- '-g "!.yarn*"',
      -- '-g "!*bundle*"'
    },
    mappings = {
      i = {
        ["<esc>"] = tel_actions.close,
        ["<C-w>"] = tel_actions_send_selected_to_qflist,
        ["<C-s>"] = tel_actions.select_horizontal,
        ["<c-q>"] = telescope_to_trouble,
        ["<C-x>"] = false
      }
    }
  },
  extensions = {
    fzf = {
      fuzzy = true, -- false will only do exact matching
      override_generic_sorter = true, -- override the generic sorter
      override_file_sorter = true, -- override the file sorter
      case_mode = "smart_case", -- or "ignore_case" or "respect_case"
      -- the default case_mode is "smart_case"
    }
  }
}

require('telescope').load_extension('fzf')

local find_command = { "fd", "--type", "f", "--hidden", "-E", ".git" }

local function tel_cmd(tel_command, opts)
  local options = {
    attach_mappings = function(_)
      tel_action_set.edit:replace(
        function(prompt_bufnr, command)
          local entry = tel_action_state.get_selected_entry()

          if not entry then
            print("[telescope] Nothing currently selected")
            return
          end

          local filename, row, col

          if entry.filename then
            filename = entry.path or entry.filename

            -- TODO: Check for off-by-one
            row = entry.row or entry.lnum
            col = entry.col
          elseif not entry.bufnr then
            -- TODO: Might want to remove this and force people
            -- to put stuff into `filename`
            local value = entry.value
            if not value then
              print("Could not do anything with blank line...")
              return
            end

            if type(value) == "table" then
              value = entry.display
            end

            local sections = vim.split(value, ":")

            filename = sections[1]
            row = tonumber(sections[2])
            col = tonumber(sections[3])
          end

          local entry_bufnr = entry.bufnr

          require("telescope.actions").close(prompt_bufnr)

          if entry_bufnr then
            actions.open_buffer(entry_bufnr, command)
          else
            local Path = require "plenary.path"
            -- check if we didn't pick a different buffer
            -- prevents restarting lsp server
            if vim.api.nvim_buf_get_name(0) ~= filename or command ~= "edit" then
              filename = Path:new(vim.fn.fnameescape(filename)):normalize(vim.loop.cwd())
              actions.open_file(filename, command, row, col)
            end
          end
        end
      )

      return true
    end
  }
  if opts ~= nil then
    options = vim.tbl_extend("force", options, opts)
  end

  return function()
    tel_command(options)
  end
end

local edit_dotfiles = tel_cmd(
  tel_builtin.find_files,
  {
    prompt_title = "~ dotfiles ~",
    shorten_path = false,
    cwd = "~/.config/dotm/profiles/noone",
    find_command = find_command
  }
)

local edit_notes = tel_cmd(
  tel_builtin.find_files,
  {
    prompt_title = "~ notes ~",
    shorten_path = false,
    cwd = "~/Notes",
    find_command = find_command
  }
)

map("n", "<leader>ff", tel_cmd(tel_builtin.find_files, { find_command = find_command }))
map("n", "<leader>fg", tel_cmd(tel_builtin.git_files))
map("n", "<leader>fa", tel_cmd(tel_builtin.live_grep))
_Search = _Search or ""
map("n", "<leader>qr", function()
  tel_cmd(
    tel_builtin.grep_string,
    {
      search = _Search
    }
  )()
end)
map(
  "n",
  "<leader>fs",
  function()
    local search = vim.fn.input("Grep For: ")
    _Search = search
    tel_cmd(
      tel_builtin.grep_string,
      {
        search = search
      }
    )()
  end
)
map("n", "<leader>fw", tel_cmd(tel_builtin.grep_string))
map("v", "<leader>fw", tel_cmd(tel_builtin.grep_string))
map("n", "<leader>fb", tel_cmd(tel_builtin.buffers, {
  sort_lastused = true,
  sort_mru = true,
  ignore_current_buffer = true,
  -- sorter = tel_sorters.get_levenshtein_sorter,
}))
map("n", "<leader>fc", tel_cmd(tel_builtin.find_files, {
  cwd = vim.fn.expand("%:p:h")
}))
map("n", "<leader>fe", tel_cmd(tel_builtin.lsp_workspace_diagnostics))
map("n", "<leader>fh", tel_cmd(tel_builtin.help_tags))
map("n", "<leader>fk", tel_cmd(tel_builtin.keymaps))
map("n", "<leader>fd", edit_dotfiles)
map("n", "<leader>fn", edit_notes)
-- }}}
-- UTIL LSP {{{
local lspconfig = require "lspconfig"
local on_attach = function(_, bufnr)
  vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")

  -- Mappings.
  local opts = { noremap = true, silent = true, buffer = bufnr }
  -- Goto
  map("n", "gd", "<cmd>lua vim.lsp.buf.definition()<CR>", opts)
  map("n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", opts)
  map(
    "n",
    "gi",
    "<cmd>lua vim.lsp.buf.implementation()<CR>",
    opts
  )

  -- Find
  map(
    "n",
    "<leader>r",
    "<cmd>lua require('telescope.builtin').lsp_references()<CR>",
    opts
  )
  map(
    "n",
    "<leader>s",
    "<cmd>lua require('telescope.builtin').lsp_document_symbols()<CR>",
    opts
  )

  -- Hover
  map("n", "K", "<cmd>lua vim.lsp.buf.hover()<CR>", opts)

  -- Code Action
  map(
    "n",
    "<leader>a",
    "<cmd>lua vim.lsp.buf.code_action()<CR>",
    opts
  )
  map(
    "v",
    "<leader>a",
    "<cmd>lua require('telescope.builtin').lsp_range_code_actions()<CR>",
    opts
  )

  -- Rename
  map(
    "n",
    "<leader>m",
    "<cmd>lua vim.lsp.buf.rename()<CR>",
    opts
  )

  -- Diagnostics
  map(
    "n",
    "<leader>d",
    "<cmd>lua vim.diagnostic.show_line_diagnostics()<CR>",
    opts
  )
  map(
    "n",
    "<leader>;",
    "<cmd>lua vim.diagnostic.goto_prev()<CR>",
    opts
  )
  map(
    "n",
    "<leader>,",
    "<cmd>lua vim.diagnostic.goto_next()<CR>",
    opts
  )

  -- Formating
  map("n", "F", "<cmd>lua vim.lsp.buf.format()<CR>", opts)
  map(
    "v",
    "F",
    "<cmd>lua vim.lsp.buf.range_formatting()<CR>",
    opts
  )
end

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)
local lsputils = require 'noone/lsputils'

require("neodev").setup({
  override = function(root_dir, library)
    if require("neodev.util").has_file(root_dir, "/etc/nixos") then
      library.enabled = true
      library.plugins = true
    end
  end,
})

local servers = {
  lua_ls = {
    cmd = { "lua-lsp" }
  },
  efm = {
    init_options = { documentFormatting = true },
    filetypes = vim.tbl_keys(lsputils.efm_languages),
    settings = {
      rootMarkers = {
        ".git/",
        ".bashrc"
      },
      languages = lsputils.efm_languages
    }
  },
  ts_ls = {
    on_attach = function(client, bufnr)
      -- Disable formatting. Using prettier via the efm server for this.
      client.resolved_capabilities.document_formatting = false
      client.resolved_capabilities.document_range_formatting = false
      on_attach(client, bufnr)
    end,
    commands = {
      OrganizeImports = {
        lsputils.typescript_organize_imports,
        description = "Organize Imports"
      }
    }
  },
  gopls = {
    settings = {
      gopls = {
        gofumpt = true
      }
    }
  },
  dockerls = {},
  yamlls = {
    settings = {
      yaml = {
        schemas = {
          kubernetes = "*.yaml",
          ["http://json.schemastore.org/github-workflow"] = ".github/workflows/*",
          ["http://json.schemastore.org/github-action"] = ".github/action.{yml,yaml}",
          ["http://json.schemastore.org/ansible-stable-2.9"] = "roles/tasks/*.{yml,yaml}",
          ["http://json.schemastore.org/prettierrc"] = ".prettierrc.{yml,yaml}",
          ["http://json.schemastore.org/kustomization"] = "kustomization.{yml,yaml}",
          ["http://json.schemastore.org/ansible-playbook"] = "*play*.{yml,yaml}",
          ["http://json.schemastore.org/chart"] = "Chart.{yml,yaml}",
          ["https://json.schemastore.org/dependabot-2.0"] = ".github/dependabot.{yml,yaml}",
          ["https://json.schemastore.org/gitlab-ci"] = "*gitlab-ci*.{yml,yaml}",
          ["https://raw.githubusercontent.com/OAI/OpenAPI-Specification/main/schemas/v3.1/schema.json"] = "*api*.{yml,yaml}",
          ["https://raw.githubusercontent.com/compose-spec/compose-spec/master/schema/compose-spec.json"] = "*docker-compose*.{yml,yaml}",
          ["https://raw.githubusercontent.com/compose-spec/compose-spec/master/schema/compose-spec.json"] = "*compose*.{yml,yaml}",
          ["https://raw.githubusercontent.com/argoproj/argo-workflows/master/api/jsonschema/schema.json"] = "*flow*.{yml,yaml}",
        },
      }
    }
  },
  jsonls = { cmd = { 'json-languageserver', '--stdio' } },
  bashls = {},
  cssls = { cmd = { 'css-languageserver', '--stdio' } },
  nixd = {},
  html =  {},
  clangd = {},
  phpactor = {},
}

for lsp, config in pairs(servers) do
  config = vim.tbl_extend("keep", config, {
    on_attach = on_attach,
    capabilities = capabilities,
    flags = {
      debounce_text_changes = 150,
    },
  })
  lspconfig[lsp].setup(config)
end
-- }}}
-- UTIL GIT {{{
map("n", "<leader>c", ":GitMessenger<CR>")
require("gitsigns").setup({
  on_attach = function(bufnr)
    local gs = package.loaded.gitsigns

    local function bmap(mode, l, r, opts)
      opts = opts or {}
      opts.buffer = bufnr
      vim.keymap.set(mode, l, r, opts)
    end

    -- Navigation
    bmap('n', ']c', function()
      if vim.wo.diff then return ']c' end
      vim.schedule(function() gs.next_hunk() end)
      return '<Ignore>'
    end, { expr = true })

    bmap('n', '[c', function()
      if vim.wo.diff then return '[c' end
      vim.schedule(function() gs.prev_hunk() end)
      return '<Ignore>'
    end, { expr = true })

    -- Actions
    bmap({ 'n', 'v' }, '<leader>hs', ':Gitsigns stage_hunk<CR>')
    bmap({ 'n', 'v' }, '<leader>hr', ':Gitsigns reset_hunk<CR>')
    bmap('n', '<leader>hS', gs.stage_buffer)
    bmap('n', '<leader>hu', gs.undo_stage_hunk)
    bmap('n', '<leader>hR', gs.reset_buffer)
    bmap('n', '<leader>hp', gs.preview_hunk)
    bmap('n', '<leader>hb', function() gs.blame_line { full = true } end)
    bmap('n', '<leader>tb', gs.toggle_current_line_blame)
    bmap('n', '<leader>hd', gs.diffthis)
    bmap('n', '<leader>hD', function() gs.diffthis('~') end)
    bmap('n', '<leader>td', gs.toggle_deleted)
  end
})

-- }}}

require("neorg").setup {
  load = {
    ["core.defaults"] = {},
    ["core.concealer"] = {},
    ["core.completion"] = {
      config = {
        engine = "nvim-cmp"
      }
    },
    ["core.dirman"] = {
      config = {
        workspaces = {
          notes = "~/notes",
        },
        default_workspace = "notes",
      },
    },
    ["core.keybinds"] = {
      config = {
        default_keybinds = true,
      }
    },
    ["core.qol.todo_items"] = {
    },
  },
}

-- LOCAL .nvimrc.lua {{{
-- Check if a ".nvimrc.lua" file exists in the current directory, if so run it.
if vim.fn.empty(vim.fn.glob(".nvimrc.lua")) == 0 then
  vim.cmd("luafile " .. ".nvimrc.lua")
end
-- }}}

-- vim:fdm=marker
