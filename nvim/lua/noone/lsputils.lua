local M = {}

local prettier = {
  formatCommand = "./node_modules/.bin/prettier --stdin-filepath ${INPUT} || prettier --stdin-filepath ${INPUT}",
  formatStdin = true
}

local eslint = {
  lintCommand = "eslint -f unix --stdin",
  lintIgnoreExitCode = true,
  lintStdin = true
}

local luafmt = {
  formatCommand = "luafmt -i 2 -l 80 --stdin",
  formatStdin = true
}

M.efm_languages = {
  javascript = { prettier, eslint },
  typescript = { prettier, eslint },
  javascriptreact = { prettier, eslint },
  typescriptreact = { prettier, eslint },
  yaml = { prettier },
  json = { prettier },
  html = { prettier },
  scss = { prettier },
  css = { prettier },
  markdown = { prettier },
  lua = { luafmt }
}

M.typescript_organize_imports = function()
  local params = {
    command = "_typescript.organizeImports",
    arguments = { vim.api.nvim_buf_get_name(0) },
    title = ""
  }
  vim.lsp.buf.execute_command(params)
end

return M
