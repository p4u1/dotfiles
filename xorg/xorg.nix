{ config, pkgs, lib, ... }:

let
  link = config.lib.file.mkOutOfStoreSymlink;
in
{
  home.file.".xsessionrc".source = link "/home/noone/.dotfiles/xorg/.xsessionrc";
}
