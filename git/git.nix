{ config, pkgs, lib, ... }:

{
  programs.git = {
    enable = true;
    ignores = [ ".nvimrc.lua" ];
    extraConfig = {
      init = {
        defaultBranch = "main";
      };
      core = {
        editor = "nvim";
      };
      url = {
        "git@github.com:" = {
          insteadOf = "https://github.com/";
        };
        "git@codeberg.org:" = {
          insteadOf = "https://codeberg.org/";
        };
      };
      color = {
        status = "auto";
        branch = "auto";
        interactive = "auto";
        ui = "true";
        pager = "true";
        diff = "auto";
      };
      pull = {
        rebase = true;
      };
    };
  };
}
