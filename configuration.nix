# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, lib, pkgs, ... }:

{
  imports =
    [
      # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Supposedly better for SSD.
  fileSystems."/".options = [ "noatime" "nodiratime" "discard" ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.copyKernels = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.grub.version = 2;
  boot.loader.grub.device = "nodev"; # /dev/sda1 or "nodev" for efi only
  boot.loader.grub.efiSupport = true;
  boot.loader.efi.canTouchEfiVariables = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";
  # Define on which hard drive you want to install Grub.
  boot.kernel.sysctl = { "net.ipv4.ip_forward" = 1; };
  boot.kernelModules = [ "kvm-amd" "kvm-intel" ];

  boot.initrd.luks.devices = {
    root = {
      device = "/dev/disk/by-uuid/4b84f7ef-1c48-44c9-957e-42ab3c238638";
      allowDiscards = true;
    };
  };


  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.  networking.useDHCP = false;
  # To list devices:
  # $ ifconfig -a
  networking.interfaces.enp0s31f6.useDHCP = true;
  networking.interfaces.wlp4s0.useDHCP = true;
  networking.interfaces.wwp0s20f0u3i12.useDHCP = true;

  networking.hostName = "nodevice"; # Define your hostname.
  networking.extraHosts =
    ''
      192.168.178.92 klasta.local
      # 192.168.121.17 klasta.local
      # 192.168.121.17 code.themom.local
    '';

  networking.nameservers = [ "8.8.4.4" ];

  networking.networkmanager.enable = true;
  networking.wireless.enable = false; # Enables wireless support via wpa_supplicant.

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 57621 ]; # For Spotify
  # networking.firewall.allowedUDPPorts = [ ... ];

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable bluetooth
  hardware.bluetooth.enable = true;
  hardware.bluetooth.settings = {
    General = {
      Enable = "Source,Sink,Media,Socket";
    };
  };
  # Bluetooth gui/applet.
  services.blueman.enable = true;

  # Enable sound.
  # hardware.pulseaudio = {
  #   enable = true;
  #
  #   # NixOS allows either a lightweight build (default) or full build of PulseAudio to be installed.
  #   # Only the full build has Bluetooth support, so it must be selected here.
  #   package = pkgs.pulseaudioFull;
  # };

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  # Set time zone.
  time.timeZone = "Europe/Amsterdam";

  # Enable virtualisation.
  virtualisation.docker.enable = true;
  virtualisation.docker.liveRestore = false;
  virtualisation.lxd.enable = true;
  virtualisation.virtualbox.host.enable = true;
  users.extraGroups.vboxusers.members = [ "noone" ];

  nixpkgs.config = {
    allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
      "spotify"
      "spotify-unwrapped"
      "hplip"
    ];

    permittedInsecurePackages = [
      "electron-14.2.9"
      "nix-2.15.3"
    ];
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    # override programs from busybox with their original version
    coreutils-full

    # Audio
    paprefs
    pavucontrol
    dconf

    # Utils
    unzip
    udiskie # For (u)mounting file systems
    usbutils
    zip
    vim
  ];
  # Need this for i3blocks
  environment.pathsToLink = [ "/libexec" ];

  nix.settings.auto-optimise-store = true;
  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 30d";
  };

  nix.package = pkgs.nixVersions.stable;
  # Enable the nix 2.0 CLI and flakes support feature-flags
  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';

  # Enable light program. This can be used to controll hardware lights (screen,
  # keyboard, ...).
  programs.light.enable = true;
  programs.gnupg.agent.enable = true;

  programs.adb.enable = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };


  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.xkb.layout = "de";
  services.xserver.xkb.options = "eurosign:e";

  # Enable touchpad support.
  services.libinput.enable = true;

  services.xserver.displayManager.lightdm.enable = true;
  services.displayManager.defaultSession = "none+i3";
  # Run udiskie as daemon, to enable auto mounting usb sticks.
  services.xserver.displayManager.sessionCommands = ''
    udiskie -s &
  '';
  services.xserver.desktopManager.gnome.enable = true;
  services.xserver.windowManager.i3.enable = true;

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Define user account.
  users.users.noone = {
    isNormalUser = true;
    extraGroups = [ "wheel" "docker" "video" "networkmanager" "libvirtd" ];
  };

  fonts.packages = with pkgs; [
    (nerdfonts.override { fonts = [ "RobotoMono" ]; })
    roboto
  ];

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "24.11"; # Did you read the comment?
}
