{ config, pkgs, lib, ... }:

let
  link = config.lib.file.mkOutOfStoreSymlink;
in
{
  home.file.".config/i3blocks/config".source = link "/home/noone/.dotfiles/i3/i3blocks.conf";
  home.file.".config/i3blocks/memory".source = link "/home/noone/.dotfiles/i3/memory";
  home.file.".config/i3blocks/battery".source = link "/home/noone/.dotfiles/i3/battery";
  home.file.".config/i3blocks/volume".source = link "/home/noone/.dotfiles/i3/volume";
  home.file.".config/i3blocks/iface".source = link "/home/noone/.dotfiles/i3/iface";
  home.file.".config/i3blocks/disk_usage".source = link "/home/noone/.dotfiles/i3/disk_usage";

  home.file.".local/bin/i3exit".source = link "/home/noone/.dotfiles/i3/i3exit";
  home.file.".local/bin/toggle-kbd-backlight".source = link "/home/noone/.dotfiles/i3/toggle-kbd-backlight";
  home.file.".local/bin/toggle-theme".source = link "/home/noone/.dotfiles/i3/toggle-theme";


  xsession.windowManager.i3 = {
    enable = true;
    config = {
      bars = [
        {
          # statusCommand = "${pkgs.i3status}/bin/i3status";
          statusCommand = "${pkgs.i3blocks}/bin/i3blocks";
          position = "bottom";
          colors = {
            background = "#222D31";
            statusline = "#F9FAF9";
            separator = "#454947";

            focusedWorkspace = { border = "#F9FAF9"; background = "#16a085"; text = "#292F34"; };
            activeWorkspace = { border = "#595B5B"; background = "#353836"; text = "#FDF6E3"; };
            inactiveWorkspace = { border = "#595B5B"; background = "#222D31"; text = "#EEE8D5"; };
            bindingMode = { border = "#16a085"; background = "#2C2C2C"; text = "#F9FAF9"; };
            urgentWorkspace = { border = "#16a085"; background = "#FDF6E3"; text = "#E5201D"; };
          };
          fonts = {
            size = 10.0;
          };
          extraConfig = ''
            ## please set your primary output first. Example: 'xrandr --output eDP1 --primary'
            #	tray_output primary
            #	tray_output eDP1

            	bindsym button4 nop
            	bindsym button5 nop
            	strip_workspace_numbers yes
          '';
        }
      ];
      keybindings = let
        modifier = "Mod4";
      in
        lib.mkDefault {};
    };
    extraConfig = ''
      # Set background image to ~/background_image.
      # This way the background image is configurable with a symlink.
      exec readlink -f ~/background_image | xargs feh --bg-scale

      # Map escape to capslock.
      exec setxkbmap -option caps:escape

      # Faster keyboard input
      exec xset r rate 500 60

      # set mod keyword to Windows key
      set $mod Mod4

      # Font for window titles. Will also be used by the bar unless a different font
      # is used in the bar {} block below.
      font pango:roboto-mono 11

      # Use Mouse+$mod to drag floating windows to their wanted position
      floating_modifier $mod

      bindsym $mod+t exec --no-startup-id ~/.local/bin/toggle-theme

      # start a terminal
      bindsym $mod+Return exec alacritty

      # kill focused window
      bindsym $mod+Shift+q kill

      # programm launcher (rofi)
      bindsym $mod+d exec rofi -show combi
      bindsym $mod+c exec rofi -show combi -run-command 'qsudo {cmd}'

      # change focus
      bindsym $mod+h focus left
      bindsym $mod+j focus down
      bindsym $mod+k focus up
      bindsym $mod+l focus right

      # move focused window
      bindsym $mod+Shift+h move left
      bindsym $mod+Shift+j move down
      bindsym $mod+Shift+k move up
      bindsym $mod+Shift+l move right

      # split in horizontal orientation
      bindsym $mod+v split h

      # split in vertical orientation
      bindsym $mod+s split v

      # enter fullscreen mode for the focused container
      bindsym $mod+f fullscreen toggle

      # change container layout (stacked, tabbed, toggle split)
      # bindsym $mod+s layout stacking
      # bindsym $mod+w layout tabbed
      # bindsym $mod+e layout toggle split

      # toggle tiling / floating
      bindsym $mod+Shift+space floating toggle

      # change focus between tiling / floating windows
      bindsym $mod+space focus mode_toggle

      # focus the parent container
      bindsym $mod+a focus parent

      # focus the child container
      #bindsym $mod+d focus child

      bindsym $mod+Shift+m move workspace to output left
      bindsym $mod+Shift+p move workspace to output right


      # switch to workspace
      bindsym $mod+1 workspace 1
      bindsym $mod+2 workspace 2
      bindsym $mod+3 workspace 3
      bindsym $mod+4 workspace 4
      bindsym $mod+5 workspace 5
      bindsym $mod+6 workspace 6
      bindsym $mod+7 workspace 7
      bindsym $mod+8 workspace 8
      bindsym $mod+9 workspace 9
      bindsym $mod+0 workspace 10

      bindsym $mod+n workspace next
      bindsym $mod+p workspace previous

      # move focused container to workspace
      bindsym $mod+Shift+1 move container to workspace 1
      bindsym $mod+Shift+2 move container to workspace 2
      bindsym $mod+Shift+3 move container to workspace 3
      bindsym $mod+Shift+4 move container to workspace 4
      bindsym $mod+Shift+5 move container to workspace 5
      bindsym $mod+Shift+6 move container to workspace 6
      bindsym $mod+Shift+7 move container to workspace 7
      bindsym $mod+Shift+8 move container to workspace 8
      bindsym $mod+Shift+9 move container to workspace 9
      bindsym $mod+Shift+0 move container to workspace 10

      # Pulse Audio controls
      bindsym XF86AudioRaiseVolume exec --no-startup-id amixer sset 'Master' 5%+
      bindsym XF86AudioLowerVolume exec --no-startup-id amixer sset 'Master' 5%-
      bindsym XF86AudioMute exec --no-startup-id amixer sset 'Master' toggle # mute sound

      # sreen brightness controls
      bindsym XF86MonBrightnessUp exec light -A 10 # increase screen brightness
      bindsym XF86MonBrightnessDown exec light -U 10 # decrease screen brightness
      bindsym XF86Explorer exec --no-startup-id ~/.local/bin/toggle-kbd-backlight # toggle keyboard backlight

      bindsym Print exec scrot

      # reload the configuration file
      bindsym $mod+Shift+c reload
      # restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
      bindsym $mod+Shift+r restart
      # exit i3 (logs you out of your X session)
      set $mode_system System (l) lock, (e) logout, (s) suspend, (h) hibernate, (r) reboot, (Shift+s) shutdown
      mode "$mode_system" {
          bindsym l exec --no-startup-id bash ~/.local/bin/i3exit lock, mode "default"
          bindsym e exec --no-startup-id bash ~/.local/bin/i3exit logout, mode "default"
          bindsym s exec --no-startup-id bash ~/.local/bin/i3exit suspend, mode "default"
          bindsym h exec --no-startup-id bash ~/.local/bin/i3exit hibernate, mode "default"
          bindsym r exec --no-startup-id bash ~/.local/bin/i3exit reboot, mode "default"
          bindsym Shift+s exec --no-startup-id bash ~/.local/bin/i3exit shutdown, mode "default"

          # back to normal: Enter or Escape
          bindsym Return mode "default"
          bindsym Escape mode "default"
      }
      bindsym $mod+shift+n exec --no-startup-id bash ~/.local/bin/i3exit lock
      bindsym $mod+shift+e mode "$mode_system"

      # resize window (you can also use the mouse for that)
      mode "resize" {
              # resize conteiners
              bindsym h resize shrink width 10 px or 10 ppt
              bindsym j resize shrink height 10 px or 10 ppt
              bindsym k resize grow height 10 px or 10 ppt
              bindsym l resize grow width 10 px or 10 ppt

              # back to normal: Enter or Escape
              bindsym Return mode "default"
              bindsym Escape mode "default"
      }

      bindsym $mod+r mode "resize"

      # Lock screen
      bindsym $mod+Delete exec --no-startup-id blurlock
      exec --no-startup-id xautolock -time 10 -corner -locker blurlock

      # exec --no-startup-id nm-applet

      # Home window
      for_window [title="unique123"] move scratchpad
      assign [title="unique123"] workspace $ws1
      bindsym $mod+minus [title="unique123"] scratchpad show
      exec_always alacritty -t unique123 -e tmuxinator start home

      # Color palette used for the terminal ( ~/.Xresources file )
      # Colors are gathered based on the documentation:
      # https://i3wm.org/docs/userguide.html#xresources
      # Change the variable name at the place you want to match the color
      # of your terminal like this:
      # [example]
      # If you want your bar to have the same background color as your 
      # terminal background change the line 362 from:
      # background #14191D
      # to:
      # background $term_background
      # Same logic applied to everything else.
      set_from_resource $term_background background
      set_from_resource $term_foreground foreground
      set_from_resource $term_color0     color0
      set_from_resource $term_color1     color1
      set_from_resource $term_color2     color2
      set_from_resource $term_color3     color3
      set_from_resource $term_color4     color4
      set_from_resource $term_color5     color5
      set_from_resource $term_color6     color6
      set_from_resource $term_color7     color7
      set_from_resource $term_color8     color8
      set_from_resource $term_color9     color9
      set_from_resource $term_color10    color10
      set_from_resource $term_color11    color11
      set_from_resource $term_color12    color12
      set_from_resource $term_color13    color13
      set_from_resource $term_color14    color14
      set_from_resource $term_color15    color15
    '';
  };


  home.packages = with pkgs; [
    python3
    iw
    acpi
    feh # Needed for the background image
    pango # Needed for fonts
    scrot # For screen capture
    xorg.xev
    i3status
    i3blocks
  ];
}
