{ config, pkgs, lib, ... }:

let
  link = config.lib.file.mkOutOfStoreSymlink;
in
{
  home.file.".bashrc".source = link "/home/noone/.dotfiles/bash/.bashrc";
  home.file.".bash_profile".source = link "/home/noone/.dotfiles/bash/bash_profile";
  home.file.".local/bin/rgr".source = link "/home/noone/.dotfiles/bash/rgr.sh";
}
