#!/bin/bash

# Enable vi mode for bash.
set -o vi

eval `keychain --quiet --agents ssh`

# Source local ~/.bashrc.local
# This file can be used for local configurations,
# that should not be tracked.
[ -f ~/.bashrc.local ] && source ~/.bashrc.local

if command -v fzf-share >/dev/null; then
  source "$(fzf-share)/key-bindings.bash"
  source "$(fzf-share)/completion.bash"
fi

# Simple prompt styling.
# Prints the current directory and a $.
# Example: "~ $"
export PS1="\W \[\e[31m\]$\[\e[0m\] "

# History size
export HISTFILE=~/.bhistfile
export HISTSIZE=20000
export HISTCONTROL=ignoredups

# Write the history after every command. This makes sure, that when entering a
# new bash sessions all commands the the currently open sessions are in the
# history.
export PROMPT_COMMAND="history -a"

replace() {
    rg "$1" --files-with-matches | xargs sed -i "s/$1/$2/g"
}

get_default_editor() {
    if [[ $(command -v nvim) != "" ]]; then
        echo "nvim"
    elif [[ $(command -v vim) != "" ]]; then
        echo "vim"
    else
        echo "vi"
    fi
}

get_fzf_default_command() {
    if [[ $(command -v rg) != "" ]]; then
        echo "rg --hidden -g '!.git' --files '.'"
    elif [[ $(command -v ag) != "" ]]; then
        echo 'ag --hidden --ignore .git -g ""'
    else 
        echo 'find | grep -v ".git"'
    fi
}

# fzf
if [[ -d "$HOME/.fzf/bin" && ! "$PATH" == *$HOME/.fzf/bin* ]]; then
    export PATH="$PATH:$HOME/.fzf/bin"
fi

if [[ $(command -v fzf) != "" ]]; then
    export FZF_DEFAULT_COMMAND=$(get_fzf_default_command)
fi

# Set default editor
export VISUAL=$(get_default_editor)
export EDITOR=$(get_default_editor)

# Set default browser for terminal applications
export BROWSER='firefox'

# Update $PATH
export PATH="$PATH:$HOME/.local/bin"
export PATH="$PATH:$HOME/.npm/bin"
export PATH="$PATH:$HOME/.yarn/bin"
export PATH="$PATH:$HOME/.config/yarn/global/node_modules/.bin"

# go
if [[ -d "/usr/local/go" ]]; then
    export GOROOT=/usr/local/go
    export PATH="$PATH:$GOROOT/bin"
fi
if [[ -d "$HOME/go" ]]; then
    export PATH="$PATH:$HOME/go/bin"
fi

# Set some aliases for convenience.
alias ls='ls --color'
alias ll='ls -alF'

alias ga='git add'
alias gs='git status'
alias gd='git diff'
alias gl='git log'
alias gch='git checkout'
alias gco='git commit'

alias dotpush='git add . && git commit -m "update" && git push'

alias bashrc='$EDITOR ~/.bashrc'
alias vimrc='$EDITOR ~/.vimrc'
alias tmuxrc='$EDITOR ~/.tmux.conf'
alias nvimrc='$EDITOR ~/.config/nvim/init.vim'
alias i3rc='$EDITOR ~/.config/i3/config'
alias todo='$EDITOR ~/todo.txt'

alias kubectl='kubecolor'

# When vim is not installed, alias it to nvim
[ ! -x $(command -v vim) ] && alias vim='nvim'

[ -x "$(command -v thefuck)" ] && eval "$(thefuck --alias)"

# Source bash completion files
[ -f /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion
[ -f /etc/bash_completion ] && . /etc/bash_completion
if [[ -d ~/.bash_completion.d ]]; then
    for file in ~/.bash_completion.d/* ; do
        source "$file"
    done
fi

# t collects all tmux sessions and tmuxinator projects, then provides a
# selection via fzf. Depending on the type that was selected, it attaches,
# switches to a tmux session or start a new tmuxinator project.
function t {
    local list
    local sessions

    # If tmux server is running, collect tmux sessions.
    if [ ! -z "$(pgrep tmux)" ]; then
        sessions=$(tmux ls | cut -d ':' -f1)

        # If we are in a session, remove the current session from list.
        if [ ! -z "$TMUX" ]; then
            curr_session=$(tmux display-message -p '#S')
            sessions=$(echo "$sessions" | sed "s/$curr_session//g")
        fi
        list="$sessions"
    fi

    # Append tmuxinator projects to list.
    for i in $(tmuxinator ls | sed '1d'); do
        list="$list $i"
    done

    selected=$(echo "$list" | sed 's/ /\n/g' | awk 'NF' | sort | uniq | fzf)

    if [[ $sessions == *"$selected"* ]]; then
        if [[ "$TMUX" != "" ]]; then
            tmux switch -t "$selected"
        else
            tmux attach -t "$selected"
        fi
    else
        tmuxinator start "$selected"
    fi
}

# p selects a project from ~/Dev and cd's into it.
function p {
    project=$(find ~/Dev/ -mindepth 3 -maxdepth 3 -type d | fzf)
    cd "$project" || exit
}
