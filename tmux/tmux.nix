{ config, pkgs, lib, ... }:

let
  link = config.lib.file.mkOutOfStoreSymlink;
in
{
  home.file.".tmux.conf".source = link "/home/noone/.dotfiles/tmux/.tmux.conf";
  home.file.".config/tmux/utils/statusbar.sh".source = link "/home/noone/.dotfiles/tmux/statusbar.sh";
  home.file.".config/tmuxinator/home.yml".source = link "/home/noone/.dotfiles/tmux/home.yml";
}
