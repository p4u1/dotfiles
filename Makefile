.PHONY: rebuild
rebuild:
	nixos-rebuild build --use-remote-sudo --flake '.#'

.PHONY: switch
switch:
	# rm ~/.config/alacritty/alacritty.yml
	nixos-rebuild switch --use-remote-sudo --flake '.#'
	setxkbmap -option caps:escape

.PHONY: update
update:
	nix flake update

.PHONY: collect-garbage
collect-garbage:
	nix-collect-garbage
