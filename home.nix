{ config, pkgs, ... }:

{
  home.sessionVariables = {
    SHELL = "\${pkgs.bash}/bin/bash";
  };

  imports = [
    ./alacritty/alacritty.nix
    ./bash/bash.nix
    ./git/git.nix
    ./i3/i3.nix
    ./nvim/neovim.nix
    ./rofi/rofi.nix
    ./tmux/tmux.nix
    ./xorg/xorg.nix
  ];

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # services.redshift.enable = true;
  # services.redshift.provider = "geoclue2";

  services.kbfs.enable = true;
  home.packages = with pkgs; [
    parted
    gparted
    unstable.rpi-imager

    # Utils
    nautilus # File Explorer
    eog # Image Viewer
    evince # Pdf Viewer
    vlc # Video Viewer
    mupdf
    calibre

    # Passwort Manager
    keepassxc

    # Image Creation
    gimp
    inkscape

    # Office
    libreoffice

    # Music
    spotify
    strawberry

    # E-Mail
    thunderbird

    # Messenger
    unstable.signal-desktop
    mumble
    element-desktop

    # Browser
    unstable.firefox
    chromium
    tor-browser-bundle-bin

    sshpass

    heimdall
    unstable.kubernetes-helm
    s3cmd

    dosbox
    # mixxx

    wireguard-tools
    tcpdump

    graphviz
    apacheHttpd

    dig
    # nextcloud-client
    openvpn

    # mysql-workbench
    arandr
    qsudo
  ];

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "noone";
  home.homeDirectory = "/home/noone";

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "24.11";
}
