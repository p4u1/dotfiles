{ config, pkgs, lib, ... }:

let
  plugins = pkgs.vimPlugins // pkgs.custom-vimPlugins;
  link = config.lib.file.mkOutOfStoreSymlink;
in
{
  home.file.".profile".source = link "/home/noone/.dotfiles/shell/profile";
}
