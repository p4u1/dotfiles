{
  description = "nixos system";

  inputs = {
    unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    stable.url = "github:nixos/nixpkgs/nixos-24.11";
    nixpkgs.follows = "stable";

    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    bats-file-0-4 = {
      url = "github:bats-core/bats-file";
      flake = false;
    };
    kustomize-latest = {
      url = "github:kubernetes-sigs/kustomize";
      flake = false;
    };
  };

  outputs = inputs:
    let
      overlay-unstable = final: prev: {
        unstable = inputs.unstable.legacyPackages.x86_64-linux;
      };
      overlay-bats = final: prev: {
        bats-file-0-4 = prev.stdenv.mkDerivation rec {
          name = "bats-file-0-4";
          src = inputs.bats-file-0-4;
          dontBuild = true;
          installPhase = ''
            mkdir -p "$out/share/bats/bats-file"
            cp load.bash "$out/share/bats/bats-file"
            cp -r src "$out/share/bats/bats-file"
          '';
        };
      };
    in
    {
      nixosConfigurations = {
        nodevice = inputs.nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            {
              nixpkgs.overlays = [
                overlay-unstable
                overlay-bats
              ];
            }
            ./configuration.nix
            inputs.home-manager.nixosModules.home-manager
            {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.users.noone = import ./home.nix;
            }
          ];
          specialArgs = { inherit inputs; };
        };
      };
    };
}
